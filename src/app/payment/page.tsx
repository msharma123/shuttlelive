// "use client";
// import { useEffect, useState } from "react";

// import { Elements,PaymentElement,useStripe,useElements } from "@stripe/react-stripe-js";
// import CheckoutForm from "../../components/CheckoutForm";
// import { StripeElementsOptions, loadStripe } from "@stripe/stripe-js";

// function Payment() {
//     //   const stripePromise= await loadStripe("pk_test_51P8hLzRtcRprcnu8H2sHt4PYgpG85BAXFlBJ037ejpgUTwzSxcoKIQUdaMkIkcThHIUq5OWjfExQ6dfk5fKa1huy00boUD0G3s") as any;
//     const clientSecret = "sk_test_51P8hLzRtcRprcnu8rDc56fWT0ecSRTtJfK1LkmuBDWVsOQaCtJcnsOxZyeidH4i5cY8OgKcozuzJJ8qG2iIShYHQ00dvGF0DWF" as string;

//     const [stripePromise, setStripePromise] = useState<any>(null);
//     // const [clientSecret, setClientSecret] = useState<string>("");

//     useEffect(() => {
//         setStripePromise(loadStripe("pk_test_51P8hLzRtcRprcnu8H2sHt4PYgpG85BAXFlBJ037ejpgUTwzSxcoKIQUdaMkIkcThHIUq5OWjfExQ6dfk5fKa1huy00boUD0G3s"));

//     }, []);

//     // useEffect(() => {
//     //     setClientSecret("sk_test_51P8hLzRtcRprcnu8rDc56fWT0ecSRTtJfK1LkmuBDWVsOQaCtJcnsOxZyeidH4i5cY8OgKcozuzJJ8qG2iIShYHQ00dvGF0DWF");
//     // }, []);

//     const appearance = {
//         theme: 'stripe',
//     };
//     const options = {
//         clientSecret: "{{sk_test_51P8hLzRtcRprcnu8rDc56fWT0ecSRTtJfK1LkmuBDWVsOQaCtJcnsOxZyeidH4i5cY8OgKcozuzJJ8qG2iIShYHQ00dvGF0DWF}}"
//     };

//     const stripe = useStripe();
//     const elements = useElements();



//     return (
//         <>
//             <h1>Stripe and the Payment Element</h1>
//             {clientSecret && stripePromise && (
//                 <Elements options={options} stripe={stripePromise}>
//                     <CheckoutForm />
//                 </Elements>
//             )}
//         </>
//     );
// }

// export default Payment;

"use client";
import React, { useState } from 'react';
import { loadStripe } from '@stripe/stripe-js';
import {
    PaymentElement,
    Elements,
    useStripe,
    useElements,
} from '@stripe/react-stripe-js';

const CheckoutForm = () => {
    const stripe = useStripe() as any;
    const elements = useElements();

    const [errorMessage, setErrorMessage] = useState<any>(null);

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (elements == null) {
            return;
        }

        // Trigger form validation and wallet collection
        const { error: submitError } = await elements.submit();
        if (submitError) {
            // Show error to your customer
            setErrorMessage(submitError.message);
            return;
        }

        // Create the PaymentIntent and obtain clientSecret from your server endpoint
        const res = await fetch('/create-intent', {
            method: 'POST',
        });

        const { client_secret: clientSecret } = await res.json();

        const { error } = await stripe.confirmPayment({
            //`Elements` instance that was used to create the Payment Element
            elements,
            clientSecret,
            confirmParams: {
                return_url: `${window.location.origin}/completion`,
            },
        });

        if (error) {
            // This point will only be reached if there is an immediate error when
            // confirming the payment. Show error to your customer (for example, payment
            // details incomplete)
            setErrorMessage(error.message as any);
        } else {
            // Your customer will be redirected to your `return_url`. For some payment
            // methods like iDEAL, your customer will be redirected to an intermediate
            // site first to authorize the payment, then redirected to the `return_url`.
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <PaymentElement />
            <button
                type="submit"
                className="text-white  bg-green-500 hover:bg-green-800   rounded  px-8 py-3 text-center mr-2 mt-5 "
                disabled={!stripe || !elements}
            >
                Pay
            </button>
            {/* Show error message to your customers */}
            {errorMessage && <div>{errorMessage}</div>}
        </form>
    );
};

const stripePromise = loadStripe('pk_test_6pRNASCoBOKtIshFeQd4XMUh');

const options = {
    mode: 'payment',
    amount: 1099,
    currency: 'usd',
    // Fully customizable with appearance API.
    appearance: {
        /*...*/
    },
};

export default function Payment() {
    return (
        <>
            <div className='flex flex-row pb-10'>
                <div className='basis-2/3 pl-5 pr-5 pt-10'>
                    <Elements stripe={stripePromise} options={options as any}>
                        <CheckoutForm />
                    </Elements>
                </div>
                <div className='basis-1/5 pl-5 pr-5'>
                </div>
            </div>
        </>
    );
}

