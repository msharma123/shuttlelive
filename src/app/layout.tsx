import type { Metadata } from "next";
import "./globals.css";
import Navbar from "@/components/NavBar";
import Footer from "@/components/Footer";
import Layout from "@/components/Layout";
import { Html } from "next/document";

export const metadata: Metadata = {
  title: "Shuttle company",
  keywords: "Shuttle Service, Sydney",
  description: "Get the best shuttle service"
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang='en'>
      <body>
        <Layout>
          {children}
        </Layout>
      </body>
    </html>
  );
}
