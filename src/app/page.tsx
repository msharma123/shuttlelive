"use client"

import Header from "@/components/Header";
import SectionService from "@/components/SectionService";
import SectionOneHomePage from "@/components/SectionOneHomePage";
import SectionTwoHomePage from "@/components/SectionTwoHomePage";
import SectionThreeHomePage from "@/components/SectionThreeHomePage";

export default function App() {
  return (
    <>
      <Header />
      <SectionService />
      <SectionOneHomePage />
      <SectionTwoHomePage />
      <SectionThreeHomePage />
    </>
  );
}
