"use client"
import React, { useMemo, useState, useRef } from 'react';
import { useLoadScript, GoogleMap, Polyline, MarkerF, CircleF, useJsApiLoader, InfoWindow } from '@react-google-maps/api';
import type { NextPage } from 'next';
import { Autocomplete, DirectionsRenderer } from '@react-google-maps/api';
import Link from "next/link";
import Payment from '../payment/page';

const Book: NextPage = () => {
    const libraries = useMemo(() => ['places'], []);
    const pricePerKm = 2;

    const [lat, setLat] = useState(27.672932021393862);
    const [lng, setLng] = useState(85.31184012689732);
    const [map, setMap] = useState<any | null>(null)
    const [directionsResponse, setDirectionsResponse] = useState<google.maps.DirectionsResult | null>(null)

    const [distance, setDistance] = useState('')
    const [duration, setDuration] = useState('')
    const [price, setPrice] = useState(0)
    const [step, setStep] = useState(1)

    const [pickupDate, setPickupDate] = useState('');
    const [pickupTime, setPickupTime] = useState('');
    const [pickupLocation, setPickupLocation] = useState('');
    const [dropoffLocation, setDropoffLocation] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');


    const center = useMemo(() => ({ lat: lat, lng: lng }), [lat, lng]);


    const mapOptions = useMemo<google.maps.MapOptions>(
        () => ({
            diableDefaultUI: true,
            clickableIcons: true,
            scrollWheel: true,
            zoomControl: false,
            streetViewControl: false,
            mapTypeControl: false,
            fullscreenControl: false,
            avoidTolls: false,
            provideRouteAlternatives: true,
        }),
        []
    );

    const { isLoaded } = useJsApiLoader({
        googleMapsApiKey: 'AIzaSyD5OhUuA4-kxLi6pfcLCgNrJY3GfUs55ec' as string,
        libraries: ['places'],
    });

    const dateRef = useRef<any>(null);
    const timeRef = useRef<any>(null);
    const originRef = useRef<any>(null);
    const firstNameRef = useRef<any>(null);
    const lastNameRef = useRef<any>(null);
    const emailRef = useRef<any>(null);
    const phoneRef = useRef<any>(null);
    const destinationRef = useRef<any>(null);
    if (!isLoaded) {
        return <p>Loading...</p>;
    }

    async function calculateRoute() {
        if (originRef.current != null && destinationRef.current != null && dateRef.current != null && timeRef.current != null) {
            if (originRef.current.value === '' || destinationRef.current.value === '' || dateRef.current.value === '' || timeRef.current.value === '') {
                return;
            }
            const directionService = new google.maps.DirectionsService();
            const results = await directionService.route({
                origin: originRef.current.value,
                destination: destinationRef.current.value,
                travelMode: google.maps.TravelMode.DRIVING,
                provideRouteAlternatives: true // Request multiple routes
            }) as any;
            if (results) {
                setDirectionsResponse(results);
                setDistance(results.routes[0].legs[0].distance.text)
                setDuration(results.routes[0].legs[0].duration.text)
                setPrice(results.routes[0].legs[0].distance?.value)
                setStep(prevState => prevState + 1);
            }
        }
    }

    function clearRoute() {
        setDirectionsResponse(null);
        setDistance('');
        setDuration('');
        setPrice(0);
        originRef.current.value = '';
        destinationRef.current.value = '';
    }

    function proceed() {
        if (step === 1) {
            setPickupDate(dateRef.current.value);
            setPickupTime(timeRef.current.value);
            setPickupLocation(originRef.current.value);
            setDropoffLocation(destinationRef.current.value);
            calculateRoute();
        } else if (step === 3) {
            setFirstName(firstNameRef.current.value);
            setLastName(lastNameRef.current.value);
            setEmail(emailRef.current.value);
            setPhone(phoneRef.current.value);
            setStep(prevState => prevState + 1);
        } else {
            setStep(prevState => prevState + 1);
        }
    }

    function back() {
        if (step === 2) {
            setStep(1);
        } else if (step === 3) {
            setStep(2);
        } else if (step === 4) {
            setStep(3);
        }
    }
    return (
        <>
            <div
                className="bg-no-repeat bg-cover bg-center  h-72"
                style={{
                    backgroundImage: `url(/image_01-4.jpg)`,
                }}
            >
                <div className="bg-black   bg-opacity-50 w-full h-full  flex items-center justify-center">
                    <p className="text-white text-5xl font-light  p-8">Book your shuttle</p>
                </div>
            </div>
            {step == 1 &&
                <div className="flex flex-col w-full max-w-4xl mx-auto bg-white">
                    <div className="flex flex-row justify-between py-4 px-8 border-b">
                        <h1 className="text-xl font-semibold">Enter Ride Details</h1>
                        <div className="flex space-x-4">
                            <div className="flex items-center justify-center w-8 h-8 rounded-full bg-orange-500 text-white">1</div>
                            <button onClick={proceed} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">2</button>
                            <button onClick={proceed} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">3</button>
                            <button onClick={proceed} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">4</button>
                        </div>
                    </div>
                    <div className="flex flex-row">
                        <div className="w-1/2 p-8">
                            <div className="space-y-4">
                                <div className="flex space-x-4">
                                    <div className="space-y-2 w-1/2">
                                        <label
                                            className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                            htmlFor="pickup-date"
                                        >
                                            Pickup Date
                                        </label>
                                        <input
                                            className="flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50"
                                            id="pickup-date"
                                            placeholder="PICKUP DATE"
                                            ref={dateRef}
                                            type="date"
                                            defaultValue={pickupDate}
                                        />
                                    </div>
                                    <div className="space-y-2 w-1/2">
                                        <label
                                            className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                            htmlFor="pickup-time"
                                        >
                                            Pickup Time
                                        </label>
                                        <input
                                            className="flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50"
                                            id="pickup-time"
                                            placeholder="PICKUP TIME"
                                            type="time"
                                            ref={timeRef}
                                            defaultValue={pickupTime}
                                        />
                                    </div>

                                </div>
                                <div className="space-y-2">
                                    <label
                                        className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                        htmlFor="pickup-location"
                                    >
                                        Pickup Location
                                    </label>
                                    <Autocomplete>
                                        <input
                                            className="flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50"
                                            id="pickup-location"
                                            placeholder="Enter a location"
                                            ref={originRef}
                                            type='text'
                                            defaultValue={pickupLocation}

                                        />
                                    </Autocomplete>
                                </div>
                                <div className="space-y-2">
                                    <label
                                        className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                        htmlFor="dropoff-location"
                                    >
                                        Dropoff Location
                                    </label>
                                    <Autocomplete>
                                        <input
                                            className="flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50"
                                            id="dropoff-location"
                                            placeholder="Enter a location"
                                            ref={destinationRef}
                                            type='text'
                                            defaultValue={dropoffLocation}
                                        />
                                    </Autocomplete>
                                </div>
                                <div style={{ display: 'flex', flexDirection: 'row' }}>
                                    <button type="button" className="text-white bg-red-500 hover:bg-red-800 rounded px-8 py-3 text-center mr-2 mt-5" onClick={clearRoute}>
                                        Clear
                                    </button>
                                    <button type="button" className="text-white bg-green-500 hover:bg-red-800 rounded px-8 py-3 text-center mr-2 mt-5" onClick={proceed}>
                                        Proceed
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="w-1/2 p-8">
                            <div className="relative">
                                <div className="absolute top-0 right-0 px-2 py-1 bg-gray-800 text-white rounded-tl-md rounded-tr-md">Map</div>
                                <GoogleMap
                                    center={center}
                                    zoom={15}
                                    mapContainerStyle={{ width: '150%', height: '350px' }}
                                    options={mapOptions}
                                    onLoad={map => { console.log(map, 'map here'); setMap(map) }}
                                >
                                    <MarkerF position={center} />
                                    {directionsResponse && directionsResponse.routes?.map((route, index) => (
                                        <DirectionsRenderer
                                            key={index}
                                            directions={directionsResponse}
                                            routeIndex={index}
                                        />
                                    ))}
                                </GoogleMap>

                            </div>
                        </div>
                    </div>
                </div>

            }
            {step == 2 &&
                <div className="flex flex-col w-full max-w-4xl mx-auto bg-white">
                    <div className="flex flex-row justify-between py-4 px-8 border-b">
                        <h1 className="text-xl font-semibold">Select your preferred route</h1>
                        <div className="flex space-x-4">
                            <button onClick={back} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">1</button>
                            <div className="flex items-center justify-center w-8 h-8 rounded-full bg-orange-500 text-white">2</div>
                            <button onClick={proceed} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">3</button>
                            <div className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">4</div>
                        </div>
                    </div>
                    <div className="flex flex-row">
                        <div className="w-1/2 p-8">
                            <div className="space-y-4">
                                {directionsResponse?.routes && directionsResponse.routes.map((route, index) => (

                                    <>
                                        <div className="space-y-2">
                                            <label
                                                className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                                htmlFor="route-1"
                                            >
                                                Route {index + 1}
                                            </label>
                                            <div className="flex items-center justify-between">
                                                <div className="flex items-center space-x-4">
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="24"
                                                        height="24"
                                                        viewBox="0 0 24 24"
                                                        fill="none"
                                                        stroke="currentColor"
                                                        strokeWidth="2"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                        className="w-5 h-5 text-orange-500"
                                                    >
                                                        <path d="M20 10c0 6-8 12-8 12s-8-6-8-12a8 8 0 0 1 16 0Z"></path>
                                                        <circle cx="12" cy="10" r="3"></circle>
                                                    </svg>
                                                    <div>
                                                        <div className="font-medium">{route.summary}</div>
                                                        <div className="text-gray-500 text-sm">{route.legs[0].duration!.text ?? ''}, {route.legs[0].distance!.text ?? ''}</div>
                                                    </div>
                                                </div>
                                                <div className="flex items-center space-x-4">
                                                    <div className="font-medium">${((route.legs[0].distance!.value / 1000) * pricePerKm).toFixed(2)}</div>
                                                    <button onClick={proceed} className="inline-flex items-center justify-center whitespace-nowrap text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 border border-input bg-background h-9 rounded-md px-3 hover:bg-orange-500 hover:text-white">
                                                        Select
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </>
                                ))}
                            </div>
                        </div>
                        <div className="w-1/2 p-8">
                            <div className="relative">
                                <div className="absolute top-0 right-0 px-2 py-1 bg-gray-800 text-white rounded-tl-md rounded-tr-md">Map</div>
                                <GoogleMap
                                    center={center}
                                    zoom={15}
                                    mapContainerStyle={{ width: '150%', height: '350px' }}
                                    options={mapOptions}
                                    onLoad={map => { console.log(map, 'map here'); setMap(map) }}
                                    mapTypeId='roadmap'
                                >
                                    <MarkerF position={center} />
                                    {directionsResponse && directionsResponse.routes?.map((route, index) => (
                                        <React.Fragment key={index}>
                                            <DirectionsRenderer
                                                directions={directionsResponse}
                                                routeIndex={index}
                                            />
                                            <Polyline
                                                path={route.overview_path}
                                                options={{
                                                    strokeColor: '#FF0000',
                                                    strokeOpacity: 0.6,
                                                    strokeWeight: 6
                                                }}
                                                key="line"
                                            />
                                            <InfoWindow
                                                position={{
                                                    lat: route.overview_path[Math.floor(route.overview_path.length / 2)].lat(),
                                                    lng: route.overview_path[Math.floor(route.overview_path.length / 2)].lng(),
                                                }}
                                                options={{
                                                    pixelOffset: new google.maps.Size(0, -30) // Adjust Y offset as needed
                                                }}
                                            >
                                                <div className='font-semibold'>
                                                    {route?.legs[0].duration!.text} <br/> A${((route.legs[0].distance!.value / 1000) * pricePerKm).toFixed(2)}
                                                </div>
                                            </InfoWindow>
                                        </React.Fragment>


                                    ))}

                                </GoogleMap>
                            </div>
                        </div>
                    </div>
                </div >
            }

            {
                step == 3 &&
                <div className="flex flex-col w-full max-w-4xl mx-auto bg-white">
                    <div className="flex flex-row justify-between py-4 px-8 border-b">
                        <h1 className="text-xl font-semibold">Enter Contact Details</h1>
                        <div className="flex space-x-4">
                            <button onClick={back} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">1</button>
                            <button onClick={back} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">2</button>
                            <div className="flex items-center justify-center w-8 h-8 rounded-full bg-orange-500 text-white">3</div>
                            <button onClick={proceed} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">4</button>
                        </div>
                    </div>
                    <div className="flex flex-row">
                        <div className="w-1/2 p-8">
                            <div className="space-y-4">
                                <div className="flex space-x-4">
                                    <div className="space-y-2 w-1/2">
                                        <label
                                            className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                            htmlFor="first-name"
                                        >
                                            First Name
                                        </label>
                                        <input
                                            className="flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50"
                                            id="first-name"
                                            placeholder="Enter First Name"
                                            type="text" ref={firstNameRef}
                                            defaultValue={firstName}
                                        />
                                    </div>
                                    <div className="space-y-2 w-1/2">
                                        <label
                                            className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                            htmlFor="last-name"
                                        >
                                            Last Name
                                        </label>
                                        <input
                                            className="flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50"
                                            id="last-name"
                                            placeholder="Enter Last Name"
                                            type="text" ref={lastNameRef}
                                            defaultValue={lastName}
                                        />
                                    </div>

                                </div>
                                <div className="space-y-2">
                                    <label
                                        className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                        htmlFor="phone-number"
                                    >
                                        Phone
                                    </label>
                                    <input
                                        className="flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50"
                                        id="phone-number"
                                        placeholder="Enter Phone Number"
                                        ref={phoneRef}
                                        type='text'
                                        defaultValue={phone}

                                    />
                                </div>
                                <div className="space-y-2">
                                    <label
                                        className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                        htmlFor="email"
                                    >
                                        Email
                                    </label>
                                    <input
                                        className="flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50"
                                        id="dropoff-location"
                                        placeholder="Enter your email"
                                        ref={emailRef}
                                        type='email'
                                        defaultValue={email}
                                    />
                                </div>
                                <div style={{ display: 'flex', flexDirection: 'row' }}>
                                    <button type="button" className="text-white bg-green-500 hover:bg-red-800 rounded px-8 py-3 text-center mr-2 mt-5" onClick={proceed}>
                                        Booking Summary
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="w-1/2 p-8">
                            <div className="relative">
                                <div className="absolute top-0 right-0 px-2 py-1 bg-gray-800 text-white rounded-tl-md rounded-tr-md">Map</div>
                                <GoogleMap
                                    center={center}
                                    zoom={15}
                                    mapContainerStyle={{ width: '150%', height: '350px' }}
                                    options={mapOptions}
                                    onLoad={map => { console.log(map, 'map here'); setMap(map) }}
                                >
                                    <MarkerF position={center} />
                                    {directionsResponse && (
                                        <DirectionsRenderer directions={directionsResponse} />
                                    )}
                                </GoogleMap>
                            </div>
                        </div>

                    </div>

                </div>
            }
            {
                step === 4 &&

                <div className="flex flex-col w-full max-w-4xl mx-auto bg-white">
                    <div className="flex flex-row justify-between py-4 px-8 border-b">
                        <h1 className="text-xl font-semibold">Booking Summary</h1>
                        <div className="flex space-x-4">
                            <button onClick={back} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">1</button>
                            <button onClick={back} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">2</button>
                            <button onClick={back} className="flex items-center justify-center w-8 h-8 rounded-full bg-gray-300">3</button>
                            <div className="flex items-center justify-center w-8 h-8 rounded-full bg-orange-500 text-white">4</div>
                        </div>
                    </div>
                    <div className="flex flex-col md:flex-row">
                        <div className="md:w-1/2 p-8">
                            <div className="space-y-4">
                                <div className="space-y-2">
                                    <div className="font-medium">Pickup</div>
                                    <div className="flex items-center space-x-2">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            stroke="currentColor"
                                            stroke-width="2"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            className="w-5 h-5 text-orange-500"
                                        >
                                            <path d="M20 10c0 6-8 12-8 12s-8-6-8-12a8 8 0 0 1 16 0Z"></path>
                                            <circle cx="12" cy="10" r="3"></circle>
                                        </svg>
                                        <div>
                                            <div className="font-medium">{directionsResponse?.request?.origin?.query}</div>
                                            <div className="text-gray-500 text-sm">4/2/2024, 10:30 AM</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="space-y-2">
                                    <div className="font-medium">Dropoff</div>
                                    <div className="flex items-center space-x-2">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            stroke="currentColor"
                                            stroke-width="2"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            className="w-5 h-5 text-orange-500"
                                        >
                                            <path d="M20 10c0 6-8 12-8 12s-8-6-8-12a8 8 0 0 1 16 0Z"></path>
                                            <circle cx="12" cy="10" r="3"></circle>
                                        </svg>
                                        <div>
                                            <div className="font-medium">{directionsResponse?.request?.destination?.query}</div>
                                            <div className="text-gray-500 text-sm">45 min, 15 miles</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="space-y-2">
                                    <div className="font-medium">Route</div>
                                    <div className="flex items-center justify-between">
                                        <div className="flex items-center space-x-2">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24"
                                                height="24"
                                                viewBox="0 0 24 24"
                                                fill="none"
                                                stroke="currentColor"
                                                stroke-width="2"
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                                className="w-5 h-5 text-orange-500"
                                            >
                                                <path d="M20 10c0 6-8 12-8 12s-8-6-8-12a8 8 0 0 1 16 0Z"></path>
                                                <circle cx="12" cy="10" r="3"></circle>
                                            </svg>
                                            <div>
                                                <div className="font-medium">{directionsResponse?.request?.origin?.query} to {directionsResponse?.request?.destination?.query}</div>
                                                <div className="text-gray-500 text-sm">45 min, 15 miles</div>
                                            </div>
                                        </div>
                                        <div className="font-medium">$25</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="md:w-1/2 p-8">
                            <div className="space-y-4">
                                <div className="space-y-2">
                                    <div className="font-medium">Ride Details</div>
                                    <div className="flex items-center justify-between">
                                        <div>Ride Type</div>
                                        <div className="font-medium">Distance</div>
                                    </div>
                                    <div className="flex items-center justify-between">
                                        <div>Estimated Time</div>
                                        <div className="font-medium">45 min</div>
                                    </div>
                                    <div className="flex items-center justify-between">
                                        <div>Estimated Distance</div>
                                        <div className="font-medium">15 miles</div>
                                    </div>
                                </div>
                                <div className="space-y-2">
                                    <div className="font-medium">Pricing</div>
                                    <div className="flex items-center justify-between">
                                        <div>Base Fare</div>
                                        <div className="font-medium">$15</div>
                                    </div>
                                    <div className="flex items-center justify-between">
                                        <div>Distance Fare</div>
                                        <div className="font-medium">$10</div>
                                    </div>
                                    <div className="flex items-center justify-between">
                                        <div>Total</div>
                                        <div className="font-medium">$25</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="flex flex-col md:flex-row justify-between py-4 px-8 border-t">
                        <button className="inline-flex items-center justify-center whitespace-nowrap rounded-md text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 border border-input bg-background hover:bg-accent hover:text-accent-foreground h-10 px-4 py-2">
                            Back
                        </button>
                        <button onClick={proceed} className="inline-flex items-center justify-center whitespace-nowrap rounded-md text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 bg-primary text-primary-foreground hover:bg-primary/90 h-10 px-4 py-2">
                            Confirm and Pay
                        </button>
                    </div>
                    <div className="flex flex-col md:flex-row w-full h-[400px] bg-gray-200 rounded-lg overflow-hidden">
                        <GoogleMap
                            center={center}
                            zoom={15}
                            mapContainerStyle={{ width: '150%', height: '350px' }}
                            options={mapOptions}
                            onLoad={map => { console.log(map, 'map here'); setMap(map) }}
                        >
                            <MarkerF position={center} />
                            {directionsResponse && (
                                <DirectionsRenderer directions={directionsResponse} />
                            )}
                        </GoogleMap>
                    </div>
                </div>
            }

            {
                
                step === 5 &&
                <div className='flex flex-row mt-5 mb-5'>
                    <div className='basis-1/2 pl-5 pr-5'>
                        <GoogleMap
                            center={center}
                            zoom={15}
                            mapContainerStyle={{ width: '100%', height: '400px' }}
                            options={mapOptions}
                            onLoad={map => { console.log(map, 'map here'); setMap(map) }}
                        >
                            <MarkerF position={center} />
                            {directionsResponse && (
                                <DirectionsRenderer directions={directionsResponse} />
                            )}
                        </GoogleMap>
                    </div>
                    <div className='basis-1/2'>
                        <h1 className='place-content-center'>Payment</h1>
                        <Payment />
                    </div>
                </div>
            }
        </>
    );
};

export default Book;
