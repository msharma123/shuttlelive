// import React, { useState } from 'react';
// import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';

// interface MapProps {
//   apiKey: string;
// }

// const Map: React.FC<MapProps> = ({ apiKey }) => {
//   const [source, setSource] = useState<google.maps.PlaceResult | null>(null);
//   const [destination, setDestination] = useState<google.maps.PlaceResult | null>(null);

//   const handlePlaceSelect = (place: google.maps.places.PlaceResult, type: 'source' | 'destination') => {
//     if (type === 'source') {
//       setSource(place);
//     } else {
//       setDestination(place);
//     }
//   };

//   return (
//     <LoadScript googleMapsApiKey={apiKey}>
//       <GoogleMap
//         mapContainerStyle={{ width: '100%', height: '400px' }}
//         zoom={8}
//         center={{ lat: 0, lng: 0 }}
//       >
//         {source && <Marker position={source.geometry.location} />}
//         {destination && <Marker position={destination.geometry.location} />}
//       </GoogleMap>
//     </LoadScript>
//   );
// };

// export default Map;
