"use client"
import { useMemo, useState } from 'react';
import { useLoadScript, GoogleMap, MarkerF, CircleF } from '@react-google-maps/api';
import type { NextPage } from 'next';
import PlacesAutocomplete from './PlacesAutoComplete';
const Home = () => {

}
// const Home: NextPage = () => {
//     const libraries = useMemo(() => ['places'], []);


//     const [lat, setLat] = useState(27.672932021393862);
//     const [lng, setLng] = useState(85.31184012689732);

//     const mapCenter = useMemo(() => ({ lat: lat, lng: lng }), [lat, lng]);

//     const mapOptions = useMemo<google.maps.MapOptions>(
//         () => ({
//             diableDefaultUI: true,
//             clickableIcons: true,
//             scrollWheel: true
//         }),
//         []
//     );

//     const { isLoaded } = useLoadScript({
//         googleMapsApiKey: 'AIzaSyD5OhUuA4-kxLi6pfcLCgNrJY3GfUs55ec' as string,
//         libraries: libraries as any,
//     });

//     if (!isLoaded) {
//         return <p>Loading...</p>;
//     }







//     return (
//         <>
//             {/*             
//             <PlacesAutocomplete
//                 onAddressSelect={(address) => {
//                     getGeocode({ address: address }).then((results) => {
//                         const { lat, lng } = getLatLng(results[0]);
//                         setLat(lat);
//                         setLng(lng);
//                     });
//                 }}
//             /> */}

//             <form className="space-y-6 mt-5 px-35" action="#" method="POST">
//                 <div>
//                     <label className="block text-sm font-medium leading-6 text-gray-900">Email address</label>
//                     <div className="mt-2">
//                         <input id="email" name="email" type="email" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
//                     </div>
//                 </div>
//                 <div>
//                     <label className="block text-sm font-medium leading-6 text-gray-900">Email address</label>
//                     <div className="mt-2">
//                         <input id="email" name="email" type="email" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
//                     </div>
//                 </div>
//                 <div>
//                     <label className="block text-sm font-medium leading-6 text-gray-900">Email address</label>
//                     <div className="mt-2">
//                         <input id="email" name="email" type="email" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
//                     </div>
//                 </div>
//                 <div>
//                     <label className="block text-sm font-medium leading-6 text-gray-900">Email address</label>
//                     <div className="mt-2">
//                         <input id="email" name="email" type="email" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
//                     </div>
//                 </div>
//                 <div>
//                     <label className="block text-sm font-medium leading-6 text-gray-900">Email address</label>
//                     <div className="mt-2">
//                         <input id="email" name="email" type="email" required className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
//                     </div>
//                 </div>

//                 <div>
//                     <button type="submit" className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Sign in</button>
//                 </div>
//             </form>




//             <GoogleMap
//                 options={mapOptions}
//                 zoom={14}
//                 center={mapCenter}
//                 mapTypeId={google.maps.MapTypeId.ROADMAP}
//                 mapContainerStyle={{ width: '400px', height: '400px' }}
//                 onLoad={() => console.log('Map Component Loaded....')}
//             >
//                 <MarkerF position={mapCenter} onLoad={() => console.log('Marker Loaded')} />

//                 {[100, 250].map((radius, idx) => {
//                     return (
//                         <CircleF
//                             key={idx}
//                             center={mapCenter}
//                             radius={radius}
//                             onLoad={() => console.log('Circle Load...')}
//                             options={{
//                                 fillColor: radius > 100 ? 'red' : 'green',
//                                 strokeColor: radius > 100 ? 'red' : 'green',
//                                 strokeOpacity: 0.8,
//                             }}
//                         />
//                     );
//                 })}
//             </GoogleMap>
//         </>

//     );
// };

export default Home;