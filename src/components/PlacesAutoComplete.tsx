import usePlacesAutocomplete from 'use-places-autocomplete';

const PlacesAutoComplete = ({
    onAddressSelect,

}: {
    onAddressSelect?: (address: string) => void;
}) => {
    const {
        ready,
        value,
        suggestions: { status, data },
        setValue,
        clearSuggestions,
    } = usePlacesAutocomplete({
        requestOptions: { componentRestrictions: { country: 'aus' } },
        debounce: 300,
        cache: 86400,
    });
    const renderSuggestions = () => {
        return data.map((suggestions) => {
            const {
                place_id,
                structured_formatting: { main_text, secondary_text },
                description,
            } = suggestions;

            return (
                <li
                    key={place_id}
                    onClick={() => {
                        setValue(description, false);
                        clearSuggestions();
                        onAddressSelect && onAddressSelect(description);
                    }}
                >
                    <strong>{main_text}</strong> <small>{secondary_text}</small>
                </li>
            )
        })
    };

return (
    <>
        <div>
                <input
                    value={value}
                    disabled={!ready}
                    onChange={(e) => setValue(e.target.value)}
                    placeholder='123 stairway to heaven'
                    className='mt-5'
                />
                {status === 'OK' && (
                    <ul>{renderSuggestions()}</ul>
                )}
            </div>
    
    </>
)
};

export default PlacesAutoComplete;