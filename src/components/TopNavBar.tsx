"use client";
import { Navbar } from "flowbite-react";
export default function TopNavbar() {
    return (
        <>
            <Navbar
            className="nav_bar_bg_color"
            >
                <Navbar.Brand href="/">
                    <img
                        src=""
                        className="mr-3 h-6 sm:h-9"
                        alt="Shuttle Logo"
                    />
                    <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">
                        ShuttleAU
                    </span>
                </Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse>
                    <Navbar.Link
                        href="/navbars"
                        active={true}
                        className="self-center"
                    >
                        Home
                    </Navbar.Link>
                    <Navbar.Link href="/navbars">
                        About
                    </Navbar.Link>
                    <Navbar.Link href="/navbars">
                        Services
                    </Navbar.Link>
                    <Navbar.Link href="/navbars">
                        Pricing
                    </Navbar.Link>
                    <Navbar.Link href="/navbars">
                        Contact
                    </Navbar.Link>
                </Navbar.Collapse>
            </Navbar>
        </>
    );
}
