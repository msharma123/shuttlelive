"use client"
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import { Autoplay, Navigation } from "swiper/modules";


const header = () => {
    return (
        <div className=" h-screen w-full" >
            <Swiper
                navigation={true}
                autoplay={{
                    delay: 2500,
                    disableOnInteraction: false,
                }}
                modules={[Autoplay, Navigation]}
            >
                <SwiperSlide>
                    <div
                        className="h-screen bg-no-repeat bg-cover bg-center"
                        style={{
                            backgroundImage: `url(/img01.jpg)`,
                        }}
                    >
                        <div className="h-screen bg-black bg-opacity-10 ">
                            <div className="h-screen flex flex-col justify-center items-center space-y-6">
                                <p className="text-7xl text-white "> Effortless Travel</p>
                                <p className="text-2xl font-light text-white ">
                                    Your Premier Shuttle Service Solution for Seamless Journeys!
                                </p>
                                <button
                                    type="button"
                                    className="text-white  bg-amber-600 hover:bg-amber-800   rounded-3xl  px-8 py-3 text-center mr-2 mb-2 "
                                >
                                    CONTACT US
                                </button>
                            </div>
                        </div>
                    </div>
                </SwiperSlide>
                <SwiperSlide>
                    <div
                        className="h-screen bg-no-repeat bg-cover bg-center"
                        style={{
                            backgroundImage: `url(/img02.jpg)`,
                        }}
                    >
                        <div className="h-screen bg-black bg-opacity-10 ">
                            <div className="h-screen flex flex-col justify-center items-center space-y-6">
                                <p className="text-7xl text-white "> Reliable Rides</p>
                                <p className="text-2xl font-light text-white ">
                                    Remarkable Experiences: Book Your Shuttle Adventure Today!
                                </p>
                                <button
                                    type="button"
                                    className="text-white  bg-amber-600 hover:bg-amber-800   rounded-3xl  px-8 py-3 text-center mr-2 mb-2 "
                                >
                                    Book Now
                                </button>
                            </div>
                        </div>
                    </div>
                </SwiperSlide>
                <SwiperSlide>
                    <div
                        className="h-screen bg-no-repeat bg-cover bg-center"
                        style={{
                            backgroundImage: `url(/img03.jpg)`,
                        }}
                    >
                        <div className="h-screen bg-black bg-opacity-10 ">
                            <div className="h-screen flex flex-col justify-center items-center space-y-6">
                                <p className="text-7xl text-white "> Arrive in Style, Depart with Ease</p>
                                <p className="text-2xl font-light text-white ">
                                    Your Go-To Shuttle Service for Stress-Free Travel!
                                </p>
                                <button
                                    type="button"
                                    className="text-white  bg-amber-600 hover:bg-amber-800   rounded-3xl  px-8 py-3 text-center mr-2 mb-2 "
                                >
                                    CONTACT US
                                </button>
                            </div>
                        </div>
                    </div>
                </SwiperSlide>
            </Swiper>
        </div >
    );
}

export default header;