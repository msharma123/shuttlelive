import {
    FaPhoneAlt,
    FaMapMarkedAlt,
    FaRegClock,
    FaAngleRight,
    FaMapMarkerAlt,
    FaPhone,
    FaPhoneSquareAlt,
    FaVoicemail,
    FaCalendarWeek,
    FaFacebook,
    FaInstagram,
    FaWhatsapp,
  } from "react-icons/fa";
  import Image from "next/image";
  import Logo from "../public/logo.png";
  const footer = () => {
    return (
      <div className="footer">
        <div className="py-16 px-16">
          <div className="container mx-auto flex flex-col space-y-24 ">
            <div className="grid lg:grid-cols-3 gap-20">
              <div className="col-span-1 flex flex-row space-x-3">
                <FaMapMarkedAlt className="w-12 h-12 text-amber-600  p-1" />
                <div className="space-y-2">
                  <p className="text-white">ADDRESS</p>
                  <p className="text-white font-medium">
                    2507 ABC, Sydney 76107
                  </p>
                </div>
              </div>
              <div className="col-span-1 flex flex-row space-x-3">
                <FaPhoneAlt className="w-12 h-12 text-amber-600  p-1" />
                <div className="space-y-2">
                  <p className="text-white">PHONES</p>
                  <p className="text-white font-medium">
                    BOOK A RIDE: (1111) 123 987 2411 OFFICE: (1111) 123 987 2412
                  </p>
                </div>
              </div>
              <div className="col-span-1 flex flex-row space-x-3">
                <FaRegClock className="w-12 h-12 text-amber-600  p-1" />
                <div className="space-y-2">
                  <p className="text-white">WORKING HOURS</p>
                  <p className="text-white font-medium">
                    MON-SAT: 07:00 - 17:00 SUN: CLOSED
                  </p>
                </div>
              </div>
            </div>
            <div className="grid lg:grid-cols-4 gap-8">
              <div className="col-span-1 space-y-8">
                {/* <Image src={Logo} alt="Logo" /> */}
  
                <p className="text-white">
                At the dawn of our journey, ShuttleAU is poised to become a leading shuttle service provider in the Sydney area, committed to excellence from the outset.
                </p>
              </div>
              <div className="col-span-1 space-y-8">
                <div className="flex flex-col space-y-2">
                  <p className="text-white font-medium">OUR SERVICES</p>
                  <p className="border-solid border border-amber-500  w-1/4 "></p>
                </div>
                <div className="flex flex-col space-y-1">
                  <div className="flex flex-row items-center space-x-2">
                    <FaAngleRight className="w-8 h-8 text-white  p-1" />
                    <p className="text-white">Experiential Tours</p>
                  </div>
                  <div className="flex flex-row items-center space-x-2">
                    <FaAngleRight className="w-8 h-8 text-white  p-1" />
                    <p className="text-white">Wedding Limousine</p>
                  </div>
                  <div className="flex flex-row items-center space-x-2">
                    <FaAngleRight className="w-8 h-8 text-white  p-1" />
                    <p className="text-white">Corporate Travel</p>
                  </div>
                  <div className="flex flex-row items-center space-x-2">
                    <FaAngleRight className="w-8 h-8 text-white  p-1" />
                    <p className="text-white">Airport Transportation</p>
                  </div>
                  <div className="flex flex-row items-center space-x-2">
                    <FaAngleRight className="w-8 h-8 text-white  p-1" />
                    <p className="text-white">Nationwide Transportation</p>
                  </div>
                </div>
              </div>
              <div className="col-span-1 space-y-8">
                <div className="flex flex-col space-y-2">
                  <p className="text-white font-medium">SHUTTLEAU</p>
                  <p className="border-solid border border-amber-500  w-1/4 "></p>
                </div>
                <div className="flex flex-col space-y-4">
                  <div className="flex flex-row items-center space-x-4">
                    <FaMapMarkerAlt className="w-6 h-6 text-white  font-light" />
                    <p className="text-white">
                      507 ABC, Sydney 76107
                    </p>
                  </div>
                  <div className="flex flex-row items-center space-x-4">
                    <FaPhone className="w-6 h-6 text-white " />
                    <p className="text-white">(1111) 123 987 2411</p>
                  </div>
                  <div className="flex flex-row items-center space-x-4">
                    <FaPhoneSquareAlt className="w-6 h-6 text-white " />
                    <p className="text-white">(1111) 123 987 2412</p>
                  </div>
                  <div className="flex flex-row items-center space-x-4">
                    <FaVoicemail className="w-6 h-6 text-white " />
                    <p className="text-white"> contact@shuttleau.com</p>
                  </div>
                  <div className="flex flex-row items-center space-x-4">
                    <FaCalendarWeek className="w-6 h-6 text-white " />
                    <p className="text-white">Mon-Sat: 07:00 - 17:00</p>
                  </div>
                </div>
              </div>
              <div className="col-span-1 space-y-8">
                <div className="flex flex-col space-y-2">
                  <p className="text-white font-medium">SUBSCRIBE</p>
                  <p className="border-solid border border-amber-500  w-1/4 "></p>
                </div>
                <div className="flex flex-col space-y-4">
                  <input
                    type="email"
                    id="email"
                    className="bg-gray-50 border border-gray-300 text-gray-900   focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 "
                    placeholder="name@example.com"
                    required
                  />
                  <button
                    type="submit"
                    className="text-white bg-amber-700 hover:bg-amber-800 focus:ring-4 focus:ring-blue-300 font-medium   w-full sm:w-auto px-5 py-2.5 text-center "
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr className=""/>
        <div className="py-8 container mx-auto flex flex-row justify-between">
          <p className="text-gray-500">Copyright 2021 ShuttleAU</p>
          <div className="flex flex-row space-x-3">
            <FaFacebook className="w-5 h-5 text-gray-500" />
            <FaInstagram className="w-5 h-5 text-gray-500" />
            <FaWhatsapp className="w-5 h-5 text-gray-500" />
          </div>
        </div>
      </div>
    );
  };
  export default footer;
  